# HW: C to ASM 
# Ayush Shresth, Emanuel Navarro-Ortiz and Nguyen Chau
# Due Sunday by 11:59pm
# gitlab url: https://gitlab.com/emannavarro/c_to_asm

sum2(int, int):                      # Sum2 label
        add     r0, r1, r0           # add register r0 and r1 and store it in r0  , (a + b) 
        bx      lr                   # branch to address proceeding address
print_the_value(int):                # print_the_value label
        mov     r1, r0               # move value from register r0 to register r1
        ldr     r0, .LCPI1_0         # load register r0 to a string 
        b       printf               # print value variable
.LCPI1_0:                            # .LCPI1_0 label  
        .long   .L.str               # loads a 32 int to a string .L.str
entry_point():                       # entry_point label
        bl      rand                 # branches to label rand which generates a psuedo ranmdom number to r0
        mov     r4, r0               # move value from register r0 to register r4, (int a)
        bl      rand                 # branches to label rand which generates a psuedo ranmdom number to r0
        mov     r1, r4               # move value from register r4 to r1, (moving int a to argument of sum2 function call )
        bl      sum2(int, int)       # branches to label sum2, function call to sum2, ( int result = sum2(b, a)) 
        bl      print_the_value(int) # bra to print_the_value and prints result.
.L.str:
        .asciz  "%d"                 # initializing a string for a 32 bit signed integer