#include <stdio.h> // header file stdio.h stands for Standard Input Output. It has the information related to input/output functions. ref: https://tinyurl.com/5h76aynr
#include <stdlib.h> //header file stdlib.h stands for Standard Library. It has the information of memory allocation/freeing functions. ref: https://tinyurl.com/4h76aynr

/*
 HW: C to ASM 
 Ayush Shresth, Emanuel Navarro-Ortiz and Nguyen Chau
 Due Sunday by 11:59pm
 gitlab url: https://gitlab.com/emannavarro/c_to_asm
*/

__attribute__((noinline))          // This function attribute prevents a function from being considered for inlining. If the function does not have side-effects, there are optimizations other than inlining that causes function calls to be optimized away, although the function call is live. To keep such calls from being optimized away. ref: https://gcc.gnu.org/onlinedocs/gcc-4.4.7/gcc/Function-Attributes.html
int sum2(int a, int b) {           // Function declaration,sum2, with return type of integer and int a param and int b param
  return a + b;                  // return the sum of para a and b
}                                  // end of sum2 function scope

__attribute__((noinline))          // This function attribute prevents a function from being considered for inlining. If the function does not have side-effects, there are optimizations other than inlining that causes function calls to be optimized away, although the function call is live. To keep such calls from being optimized away. ref: https://gcc.gnu.org/onlinedocs/gcc-4.4.7/gcc/Function-Attributes.html
void print_the_value(int value) {  // Function declaration,print_the_value, with void return type(returns nothing)
  printf("%d", value);           // print function declaration of Signed decimal integer
}                                  // end of print_the_value function scope

int entry_point() {                // function declaration,entry_point, with return type of integer
  int a = rand();                // declaration of int a, a pseudo-random number in the range of 0 to RAND_MAX. refrence: https://tinyurl.com/2d5s7dw9
  int b = rand();                // declaration of int b, a pseudo-random number in the range of 0 to RAND_MAX.
  int result = sum2(b, a);       //declaration of int result and a function call of sum2 function that takes variable b and variable a arguments and return the sum of two signed ints.
  print_the_value(result);       // function call of print_the_value with argument of result which will print the value of result
}                                  // end of entry_point function scop
